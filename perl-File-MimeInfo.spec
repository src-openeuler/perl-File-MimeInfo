#%global debug_package %{nil}

Name:		perl-File-MimeInfo
Version:	0.35
Release:	3
Summary:	Implement the freedesktop specification

License:	GPL-1.0-or-later or Artistic-1.0
URL:		https://metacpan.org/release/File-MimeInfo
Source0:	https://cpan.metacpan.org/modules/by-module/File/File-MimeInfo-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  findutils make
BuildRequires:	perl-interpreter perl-generators
BuildRequires:	perl(Exporter)
BuildRequires:	perl(Fcntl)
BuildRequires:	perl(Pod::Usage)
BuildRequires:	perl(File::Basename)
BuildRequires:	perl(File::BaseDir)
BuildRequires:	perl(File::DesktopEntry)
BuildRequires:	shared-mime-info
BuildRequires:  perl(Encode::Locale)

Requires:	perl(File::DesktopEntry) >= 0.04
Requires:	perl(File::BaseDir) >= 0.03

%description
This module can be used to determine the mime type of a file; it's a replacement
for File::MMagic trying to implement the freedesktop specification for using the
shared mime-info database. The package comes with a script called mimetype that 
can be used as a file(1) work-alike.

%package help
Summary: Help documents for perl-File-MimeInfo
%description help
The help documents for perl-File-MimeInfo

%prep
%setup -q -n File-MimeInfo-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor
%make_build

%install
%make_install DESTDIR=$RPM_BUILD_ROOT

%check
make test

%files
%{_bindir}/mimeopen
%{_bindir}/mimetype
%{perl_vendorlib}/*

%files help
%doc Changes
%{_mandir}/man1/*
%{_mandir}/man3/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 0.35-3
- drop useless perl(:MODULE_COMPAT) requirement

* Mon Dec 30 2024 wangkai <13474090681@163.com> - 0.35-2
- Delete unneeded files

* Wed Sep 25 2024 wangkai <13474090681@163.com> - 0.35-1
- Update to 0.35

* Wed Jul 17 2024 yangchenguang <yangchenguang@kylinsec.com.cn> - 0.33-2
- Modify /usr/lib64/ to %{_libdir}

* Thu Mar 23 2023 wenchaofan <349464272@qq.com> - 0.33-1
- Update version to 0.33

* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 0.32-1
- Upgrade to version 0.32

* Tue Jan 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.29-3
- mark as noarch

* Tue Jan 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.29-2
- Fix build requirements

* Tue Jan 7 2020 shenkai <shenkai8@huawei.com> - 0.29-1
- Package init
